from django.shortcuts import render
from django.http import HttpResponse

import random

import urllib, json

#importing customer model
from testApp.models import Customers

#experamenting with reading bus api
def index(request):
    with urllib.request.urlopen("https://rtl2.ods-live.co.uk//api/vehiclePositions?key=") as url:
        data = json.loads(url.read().decode())
    vehicleNumbers = []
    for element in data:
        vehicleNumbers.append(element["vehicle"])
    return HttpResponse(str(vehicleNumbers))

#experamenting with reading bus api
def index2(request):
    with urllib.request.urlopen("https://rtl2.ods-live.co.uk//api/vehiclePositions?key=") as url:
        data = json.loads(url.read().decode())
    vehicleNumbers = []
    for element in data:
        vehicleNumbers.append(element["latitude"])
    return HttpResponse(str(vehicleNumbers))

#view that returns data held by Customers model to webpage with path " "
def index3(request):
    return HttpResponse(Customers.objects.all())