from django.urls import path
from . import views 

urlpatterns = [
    path("", views.index3),
    path("latitude", views.index2),
    path("vehicles", views.index),
]
